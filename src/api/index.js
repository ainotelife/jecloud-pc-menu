/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';

import {
  API_MENU_TREE,
  API_MENU_DETAIL,
  API_MENU_DOSAVE,
  API_MENU_DOUPDATE,
  API_MENU_DOREMOVE,
  API_MENU_MENUMOVE,
  API_MENU_RESETSTATICIZE,
  API_MENU_DODEVELOPPERM,
  API_MENU_PRODUCTLOAD,
  API_MENU_MOVE,
  GET_FUNCTION_LOADGRIDTREE,
  GET_PRODUCT_DATA,
} from './urls';

/**
 * 获取产品接口数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getProductData(params) {
  return ajax({ url: GET_PRODUCT_DATA, params }).then((info) => {
    if (info.success) {
      return info.data.rows;
    } else {
      return Promise.reject(info);
    }
  });
}
/**
 * 获取子系统tree数据
 * @export
 * @param {String} 表编码 tableCode
 * @param {String} SQL查询扩展 j_query
 * @param {String} SQL查询扩展rootId
 * @param {String} 产品ID   SY_PRODUCT_ID
 * @return {Promise}
 */
export function GetFunctionTreeData(param) {
  const params = {
    tableCode: 'JE_CORE_FUNCINFO',
    rootId: 'ROOT',
    SY_PRODUCT_ID: param.SY_PRODUCT_ID,
    j_query: JSON.stringify({
      order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
      custom: [
        {
          type: 'and',
          value: [
            { type: 'like', code: 'FUNCINFO_FUNCNAME', value: param.searchValue, cn: 'or' },
            { type: 'like', code: 'FUNCINFO_FUNCCODE', value: param.searchValue, cn: 'or' },
          ],
        },
      ],
    }),
  };
  return ajax({ url: GET_FUNCTION_LOADGRIDTREE, params: { ...params } }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 菜单树
 *
 * @export
 * @return {Object}
 */
export function getMenuTree(params) {
  return ajax({ url: API_MENU_TREE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 菜单详细信息
 *
 * @export
 * @return {Object}
 */
export function getMenuDetail(params) {
  return ajax({ url: API_MENU_DETAIL, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 保存菜单
 *
 * @export
 * @return {Object}
 */
export function doSaveMenu(params) {
  return ajax({ url: API_MENU_DOSAVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 更新菜单
 *
 * @export
 * @return {Object}
 */
export function doUpdateMenu(params) {
  return ajax({ url: API_MENU_DOUPDATE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除菜单
 *
 * @export
 * @return {Object}
 */
export function doRemoveMenu(params) {
  return ajax({ url: API_MENU_DOREMOVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 移动菜单
 *
 * @export
 * @return {Object}
 */
export function doMenuMove(params) {
  return ajax({ url: API_MENU_MENUMOVE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 清空菜单缓存
 *
 * @export
 * @return {Object}
 */
export function doResetStaticize(params) {
  return ajax({ url: API_MENU_RESETSTATICIZE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 授权给开发人员
 *
 * @export
 * @return {Object}
 */
export function doDevelopPerm(params) {
  return ajax({ url: API_MENU_DODEVELOPPERM, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 产品列表
 *
 * @export
 * @return {Object}
 */
export function getProDuct(params) {
  return ajax({ url: API_MENU_PRODUCTLOAD, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 拖拽
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function moveTree(params) {
  return ajax({ url: API_MENU_MOVE, params: params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
