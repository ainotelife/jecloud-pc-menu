/**
 * API_URL命名规则：API_模块_方法
 */

// 菜单树
export const API_MENU_TREE = '/je/rbac/menu/getTree';

// 菜单详细信息
export const API_MENU_DETAIL = '/je/rbac/menu/getInfoById';

// 提交菜单
export const API_MENU_DOSAVE = '/je/rbac/menu/doSave';

// 更新菜单
export const API_MENU_DOUPDATE = '/je/rbac/menu/doUpdate';

// 删除菜单
export const API_MENU_DOREMOVE = '/je/rbac/menu/doRemove';

// 移动菜单
export const API_MENU_MENUMOVE = '/je/rbac/menu/menuMove';

// 清空菜单缓存
export const API_MENU_RESETSTATICIZE = '/je/rbac/menu/resetMenuStaticize';

// 授权给开发人员
export const API_MENU_DODEVELOPPERM = '/je/rbac/permission/quickGranMenuPerm';

// 产品列表
export const API_MENU_PRODUCTLOAD = '/je/meta/product/load';

// 拖拽
export const API_MENU_MOVE = 'je/rbac/menu/move';

// 获取子系统列表数据
export const GET_FUNCTION_LOADGRIDTREE = '/je/meta/funcInfo/loadGridTreeData';

// 获取产品的数据
export const GET_PRODUCT_DATA = '/je/meta/product/load';
