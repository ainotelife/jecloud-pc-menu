# [2.1.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.9...v2.1.0) (2023-10-20)


### Bug Fixes

* **http:** 优化http请求的拦截器和退出登录的逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([61178f3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/61178f33af0d27bbd0a7f1798e6e6eba8209810c))
* **update:** 选择功能报错问题[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([954d69f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/954d69fd05518a0993463ae9a07f5c0a9ba2dae7))


### Features

* **version:** release v2.0.8 ([8970821](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/89708211d43becbdbff332f64005cf60c108b366))
* **version:** release v2.0.9 ([0fbfd4a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/0fbfd4add77a87758df961c9e95ef2e3687a307f))



## [2.0.9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.8...v2.0.9) (2023-09-22)



## [2.0.8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.7...v2.0.8) (2023-09-15)


### Bug Fixes

* **env:** 更新服务代理地址[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([25bafcb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/25bafcb0e894b9a126d1b7067e8ab7edf2e81cc5))


### Features

* **doc:** 文件changelog修改提交 ([0028b1a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/0028b1a8b43fc25201838cb86d94951dba5fe1ac))
* **script:** 增加setup:lib命令，提供非源码客户使用[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([a2911c9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a2911c9b761941d3af6f7e73b5e7fb91f8aff203))
* **version:** release v2.0.4 ([abfadad](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/abfadad80aac3a9f264dff1c2c861d9dbcc0ffc8))
* **version:** release v2.0.5 ([14f06f4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/14f06f42d697b2fcd4132d72e9348059593347d5))
* **version:** release v2.0.6 ([75f479d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/75f479d97a391a8e43078bbae15e0d82c21b69a5))
* **version:** release v2.0.6 ([0e84c35](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/0e84c358a52e1ca7df1040f58dfad18348faaa56))
* **version:** release v2.0.7 ([ca2a996](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/ca2a9961629a05cd7e9f4e8978e3446a558ce3e3))



## [2.0.7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.6...v2.0.7) (2023-09-08)



## [2.0.6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.5...v2.0.6) (2023-09-01)



## [2.0.5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.4...v2.0.5) (2023-08-25)


### Features

* **menu:** 菜单添加数据弹窗加loading效果[[#3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/3)] ([aff8b8c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/aff8b8cc89bc592ae6f10ad141fc3165b11d3ac1))



## [2.0.4](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v2.0.3...v2.0.4) (2023-08-18)



## [2.0.3](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v1.4.0...v2.0.3) (2023-08-12)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 修复websocket代理地址失效问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([6975273](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/69752730079a6686f1be71e01f454f4f24f07609))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **menu:** 数据取数据字典，添加插件数据过滤[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([99a250f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/99a250fe4dd48dd8b6efda84785f93dca32a179a))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **update:** 菜单中功能类型文案变成功能展示类型[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2e00127](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2e0012747a23919797d7b9c6e36536837270c75b))
* **update:** 测试高德地图[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2021cf6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2021cf6cddc8c343dc71c5c9168845e51224c9b9))
* **update:** 测试视频案例代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([a07513e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a07513e8da953bea4ee9c2d270b6571c73e985ad))
* **update:** 地图插件冗余代码删除[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([1528e15](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/1528e1571a5000406c8deb3ab474969f1f8d1e43))
* **update:** 地图组件处理[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([77c93cb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/77c93cb3be12243e912bfbcb25143046b05225aa))
* **update:** 解决语法报错问题[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([049348c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/049348ce3bff20100ef2d6b5dc6ddc34b8a85a65))
* **update:** 引入地图资源[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([e60544a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e60544a7dd0deaa2e5b39e81fc3555259b4ff457))
* **update:** 引入map[[#25](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/25)] ([5b61511](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/5b615114eef425d24c2587ff19d2e868568ff112))
* **update:** 暂存代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([30d087c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/30d087ccfb78aa7eb5e8fb87adfe83a479ffbeaa))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([2eeecfc](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2eeecfceee43fa69d84f7689d90ef173de5a193f))
* **version:** release v2.0.0 ([f880732](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f8807323f1ecdf76575dc1fbe1cd452f93cd8f33))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([fd31ba7](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/fd31ba799356403a25b62f549e9a6c899c8697f3))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))
* **version:** release v2.0.3 ([1793e6b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/1793e6b39724654d98563036fade36e4282d8b14))
* **version:** releasse v2.0.1 ([b3bb481](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b3bb481c62857e11ca4819eac8836fbf319e5a6d))



## [2.0.2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v1.4.0...v2.0.2) (2023-08-04)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **menu:** 数据取数据字典，添加插件数据过滤[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([99a250f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/99a250fe4dd48dd8b6efda84785f93dca32a179a))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **update:** 菜单中功能类型文案变成功能展示类型[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2e00127](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2e0012747a23919797d7b9c6e36536837270c75b))
* **update:** 测试高德地图[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2021cf6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2021cf6cddc8c343dc71c5c9168845e51224c9b9))
* **update:** 测试视频案例代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([a07513e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a07513e8da953bea4ee9c2d270b6571c73e985ad))
* **update:** 地图插件冗余代码删除[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([1528e15](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/1528e1571a5000406c8deb3ab474969f1f8d1e43))
* **update:** 地图组件处理[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([77c93cb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/77c93cb3be12243e912bfbcb25143046b05225aa))
* **update:** 解决语法报错问题[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([049348c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/049348ce3bff20100ef2d6b5dc6ddc34b8a85a65))
* **update:** 引入地图资源[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([e60544a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e60544a7dd0deaa2e5b39e81fc3555259b4ff457))
* **update:** 引入map[[#25](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/25)] ([5b61511](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/5b615114eef425d24c2587ff19d2e868568ff112))
* **update:** 暂存代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([30d087c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/30d087ccfb78aa7eb5e8fb87adfe83a479ffbeaa))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([2eeecfc](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2eeecfceee43fa69d84f7689d90ef173de5a193f))
* **version:** release v2.0.0 ([f880732](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f8807323f1ecdf76575dc1fbe1cd452f93cd8f33))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d9b316b2671fb760e1456907c216adff855d4537))
* **version:** release v2.0.2 ([4e122ac](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/4e122ac6eb02547e9e39c9a7921fd254b568dfb4))
* **version:** releasse v2.0.1 ([b3bb481](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b3bb481c62857e11ca4819eac8836fbf319e5a6d))



## [2.0.1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v1.4.0...v2.0.1) (2023-07-30)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加自动生成公共资源输出目录[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b2c5562](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b2c55629a9a09f76ba0da4874035a5e024d9d6c2))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **menu:** 数据取数据字典，添加插件数据过滤[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([99a250f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/99a250fe4dd48dd8b6efda84785f93dca32a179a))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **update:** 菜单中功能类型文案变成功能展示类型[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2e00127](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2e0012747a23919797d7b9c6e36536837270c75b))
* **update:** 测试高德地图[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2021cf6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2021cf6cddc8c343dc71c5c9168845e51224c9b9))
* **update:** 测试视频案例代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([a07513e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a07513e8da953bea4ee9c2d270b6571c73e985ad))
* **update:** 地图插件冗余代码删除[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([1528e15](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/1528e1571a5000406c8deb3ab474969f1f8d1e43))
* **update:** 地图组件处理[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([77c93cb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/77c93cb3be12243e912bfbcb25143046b05225aa))
* **update:** 解决语法报错问题[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([049348c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/049348ce3bff20100ef2d6b5dc6ddc34b8a85a65))
* **update:** 引入地图资源[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([e60544a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e60544a7dd0deaa2e5b39e81fc3555259b4ff457))
* **update:** 引入map[[#25](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/25)] ([5b61511](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/5b615114eef425d24c2587ff19d2e868568ff112))
* **update:** 暂存代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([30d087c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/30d087ccfb78aa7eb5e8fb87adfe83a479ffbeaa))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([2eeecfc](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2eeecfceee43fa69d84f7689d90ef173de5a193f))
* **version:** release v2.0.0 ([f880732](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f8807323f1ecdf76575dc1fbe1cd452f93cd8f33))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))
* **version:** release v2.0.1 ([d9b316b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d9b316b2671fb760e1456907c216adff855d4537))



# [2.0.0](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/compare/v1.4.0...v2.0.0) (2023-07-22)


### Bug Fixes

* **build:** 添加微应用的代理地址配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([45555b9](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/45555b9edf9317cd069efa1964bd202fcb121479))
* **build:** 增加websocket代理配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([867e0e8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/867e0e8eb850a202935224b852fb753ba1df0788))
* **code:** 删除无效代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([027ba01](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/027ba01d689190651034a29845bbfd8b80c1fbe5))
* **doc:** 更新说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([c5e53de](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5e53de33c5a2cf7ce353f85799b9bfe8522db98))
* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **doc:** 增加说明文档 ([35284f8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/35284f827d6a28fd9001b260dc412c8d3d833fe6))
* **env:** 优化系统变量配置[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([4a1662d](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/4a1662d27db94fac6f07bc8b6abe8dd07e0b9e62))
* **menu:** 数据取数据字典，添加插件数据过滤[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([99a250f](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/99a250fe4dd48dd8b6efda84785f93dca32a179a))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **package:** 更新邮箱信息 ([599b555](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/599b555b5e780d5ab21348717a6895ec6a036aab))
* **style:** 调整html样式引用顺序，导致主题失效[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([a9117fd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a9117fda4d9a5f5476b2089f419a98f2af238289))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **theme:** 删除测试代码[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([5690210](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/56902100a501e29589a635f8d6b9b0f3e598e053))
* **update:** 菜单中功能类型文案变成功能展示类型[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2e00127](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2e0012747a23919797d7b9c6e36536837270c75b))
* **update:** 测试高德地图[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([2021cf6](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2021cf6cddc8c343dc71c5c9168845e51224c9b9))
* **update:** 测试视频案例代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([a07513e](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/a07513e8da953bea4ee9c2d270b6571c73e985ad))
* **update:** 地图插件冗余代码删除[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([1528e15](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/1528e1571a5000406c8deb3ab474969f1f8d1e43))
* **update:** 地图组件处理[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([77c93cb](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/77c93cb3be12243e912bfbcb25143046b05225aa))
* **update:** 解决语法报错问题[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([049348c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/049348ce3bff20100ef2d6b5dc6ddc34b8a85a65))
* **update:** 引入地图资源[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([e60544a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e60544a7dd0deaa2e5b39e81fc3555259b4ff457))
* **update:** 引入map[[#25](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/25)] ([5b61511](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/5b615114eef425d24c2587ff19d2e868568ff112))
* **update:** 暂存代码[[#5](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/5)] ([30d087c](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/30d087ccfb78aa7eb5e8fb87adfe83a479ffbeaa))


### Features

* **build:** 增加AJAX_BASE_URL变量，可以动态修改[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([f7b9640](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/f7b96406849c4aacaa76e5f0310b43bf1614cb1f))
* **license:** 增加开源协议文件 ([d8ef8c2](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/d8ef8c28796f3e86343dcb3a21a3fb2567f175a5))
* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **theme:** 系统主题采用css变量形式[[#8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/issues/8)] ([59545bd](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/59545bdde0b85221946596f942fe62f0a16a1076))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))
* **version:** release v1.4.1 ([e189132](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/e189132f14b749c4bc7740d1b9a7b8d9a15e45c7))
* **version:** release v1.4.1 ([2eeecfc](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/2eeecfceee43fa69d84f7689d90ef173de5a193f))
* **version:** release v2.0.0 ([c5bcde8](http://gitlab.suanbanyun.com/jecloud/opensource/frontend/jecloud-pc-menu/commit/c5bcde88fb1664fd850d5d19cb4a2f8d7c101d63))



## [1.4.1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/v1.4.0...v1.4.1) (2023-06-30)


### Bug Fixes

* **doc:** 修改开发技巧文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([47340c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/47340c108cadae06bbe0526ed10cba18a0245de7))
* **micro:** 增加render的同步支持[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([bb577a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bb577a6cc316d68c522ff0a0409f610ba45552c0))
* **system:** 修复系统缓存数据更新异常问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([dcc37bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/dcc37bbcca2b8b4d92ce8a782bd8096f33732d5a))
* **update:** 菜单中功能类型文案变成功能展示类型[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/5)] ([2e00127](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2e0012747a23919797d7b9c6e36536837270c75b))


### Features

* **micro:** 微应用支持JE.useAdmin()调用主应用的函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([b59153b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b59153b59d379e2155a14166f04e9a53ea3c703d))
* **version:** release v1.4.0 ([2463d7a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2463d7a0e71d583a4d0ffda2fd120b9fd7e25f7a))



# [1.4.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/v1.2.0...v1.4.0) (2023-06-06)


### Bug Fixes

* **ajax:** 修复主子应用共用ajax实例[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([94f40c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/94f40c21f97664e3f0150830f1e6e93ed1849aa9))
* **plugin:** 废弃@jecloud/plugin,调整JE的注册[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([052a918](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/052a918eec2e9cdab556e52fbecc727384ebecd4))
* **update:** 授权给开发人员[[#25](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/25)] ([0918800](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0918800b92e0ee331f7564602de0be4571add9df))
* **update:** 添加选择功能搜索值为空的逻辑[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/5)】 ([ba7e772](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ba7e77283cb4fdb27a4f993b00a96890119406dd))
* **update:** 选择功能搜索框样式修复[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/5)] ([2605ecb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2605ecbb051393886364cf17a2e521e2644313e2))
* **update:** 选择功能页面产品联系上下文[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/5)] ([614aa23](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/614aa2349cf7a69a573c3a7c9c3ab4c569e2d815))
* **utils:** @jecloud/utils删除vue依赖[[#67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/67)] ([e6929e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e6929e0d0df43b58c15f3bdd41d20d525f30fa20))
* **utils:** 适配jecloud/utils的调整 ([47b8cdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/47b8cdd79cfd1d2a459f809a5ebbf8bcd11ce8e5))


### Features

* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 图片预览区分节点加载[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/11)] ([2876a2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2876a2eed3a5e13047094d4034aff39e88bebfcf))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/affc489c088b3b77dbd2d176a887562726735540))
* **je:** 暴露JE常用方法 ([4efb75b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4efb75b4881eae4000153638e1d0d4b4628547cb))
* **je:** 将vue的h函数添加到JE上 ([753fad9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/753fad92b4fbf4a7aabbcc29fdd3b950ad3f426a))
* **menu:** 菜单描述和样式调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([2b1fcf2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2b1fcf2e15fb6c78273fec1ac55ee6a894e5de20))
* **menu:** 菜单描述和样式调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([303ca4f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/303ca4f38bfb0056de2f0411d20aaad577ba1ee6))
* **menu:** 菜单选择字典只能选列表和树形的[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([1393303](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/1393303cc227d31f165b655cdb61d5ed4b304251))
* **menu:** 功能类型下增加启用加载第一条数据字段[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/4)] ([c4b100b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c4b100be8335a97c2025eeb52f01dc6f30edf549))
* **menu:** 关联顶部菜单变为多选[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([9b152c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9b152c0c27aaadfcdb6711c983647b833f53318c))
* **menu:** 关联顶部菜单查询选择传querys[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([912eec6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/912eec6775731152168548289e04a40fe5377467))
* **menu:** 增加图表菜单及更改门户图表菜单配置[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/4)] ([31cbfbd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/31cbfbd53f5ec75112ef936eb6c501caf4378f6d))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **version:** release v1.3.0 ([0f6a9a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0f6a9a668b51b4557fdc0ce939e6dfc04439c97c))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.3.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/v1.2.0...v1.3.0) (2023-05-05)


### Bug Fixes

* **update:** 添加选择功能搜索值为空的逻辑[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/5)】 ([ba7e772](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ba7e77283cb4fdb27a4f993b00a96890119406dd))
* **update:** 选择功能搜索框样式修复[[#5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/5)] ([2605ecb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2605ecbb051393886364cf17a2e521e2644313e2))


### Features

* **archetype:** 登录成功后增加制定路径跳转方式[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/11)] ([4cb2284](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4cb2284919221aac7c2ed5fabc2b4c57f502150d))
* **archetype:** 增加文件整合静态资源[[#11](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/11)] ([0a88993](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0a8899373879f5e2f8925e61f5656e544c476ce4))
* **cli:** 自定义路由提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/10)] ([6c14ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6c14ac327bd4b3cb138cc42a9b476a0821a9b55b))
* **code:** 增加支持全局脚本库 ([affc489](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/affc489c088b3b77dbd2d176a887562726735540))
* **menu:** 菜单描述和样式调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([2b1fcf2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2b1fcf2e15fb6c78273fec1ac55ee6a894e5de20))
* **menu:** 菜单描述和样式调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([303ca4f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/303ca4f38bfb0056de2f0411d20aaad577ba1ee6))
* **menu:** 功能类型下增加启用加载第一条数据字段[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/4)] ([c4b100b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c4b100be8335a97c2025eeb52f01dc6f30edf549))
* **menu:** 增加图表菜单及更改门户图表菜单配置[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/4)] ([31cbfbd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/31cbfbd53f5ec75112ef936eb6c501caf4378f6d))
* **service:** 接口参数修改 ([c14f074](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c14f074cfdfbbb17f5a94083bb0273a5bd2aa90d))
* **version:** release v1.3.0 ([379dfe6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/379dfe68892583c042d0e92a33d5db745f9fa629))



# [1.2.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/v1.0.2...v1.2.0) (2023-04-06)


### Bug Fixes

* **build:** 修复样式文件采用参数引用方式 ([dc6fd3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/dc6fd3fecc0c02b536590915888f967333cdce44))
* **font:** 修复字体文件为版本参数引用 ([a353a02](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a353a02d0cf04447a92ae635604b4545a6a18f63))
* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ae5b45c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))


### Features

* **cli:** changelog提交 ([0a4c001](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0a4c001c14276bacd26ff9dd3b5798fab203a459))
* **cli:** packagelock文件提交 ([9282afd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9282afd0bc17b871511e3d59bf7f9f2fee378440))
* **cli:** v1.1.0定版 ([11af62b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/11af62bd47230ee6d422effa56e4dd446b0ecca0))
* **init:** 增加初始化加载系统配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([a2820a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/10)] ([75c0262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/75c0262173c62362ad83ef5be7dfef578ae159de))
* **menu:** changelog提交 ([f188f62](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f188f6256f71c7a8fe3efc43d4ed337712bd10ff))
* **menu:** v1.1.0定版 ([8f1d4b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/8f1d4b575a5e58c0c32c62cf0af64095ea3e0493))
* **pdf:** pdf预览资源 ([530c87e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/530c87e6eea990882732446b3ca3a8f74310968a))



# [1.1.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/v1.0.2...v1.1.0) (2023-03-03)


### Bug Fixes

* **i18n:** 修改欢迎页i18n文字，变量导致火狐52卡死[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ae5b45c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ae5b45c45bc02fb81f0e80df6ad1b84686f0e7c4))


### Features

* **init:** 增加初始化加载系统配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([a2820a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a2820a46173411a049263d9358473f79f6d6e64d))
* **md:** 火狐52浏览器适配文档提交 ([6b5c00b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6b5c00b76bcff13164ce0f79364c1f06f7afc882))
* **md:** 火狐52浏览器适配文档提交[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/10)] ([75c0262](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/75c0262173c62362ad83ef5be7dfef578ae159de))
* **menu:** v1.1.0定版 ([8f1d4b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/8f1d4b575a5e58c0c32c62cf0af64095ea3e0493))



## [1.0.2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/v1.0.0...v1.0.2) (2023-01-10)


### Bug Fixes

* **code:** 调整代码，适应所有分支结构 ([2e7d448](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2e7d44879680fda6cf2af72b7c6b1fd4cc18ed5c))
* **icon:** 升级图标配置 ([ae975c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ae975c4dec20dcb889971157953f7227826c56ab))
* **logout:** 修复退出登录接口[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/9)] ([e244604](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e244604c6aa1bfd01268e4cbf35af8e7a82afc44))
* **logout:** 增加退出登录接口调用[[#9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/9)] ([bda04a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bda04a21469159c1973ab121b15c49c81f838425))
* **npm:** 废弃pnpm安装，采用原始npm安装依赖[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([9e653ec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9e653ec168b9b0ce44aafb2f59c656ba1f2811cd))
* **npm:** 更新npm lock文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([11cbdc5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/11cbdc56dafbc7f0ada60ac8b0bf8716840ef493))
* **npm:** 修改npm私服地址 ([cb61aad](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/cb61aadb4077424573b8b7e8770972d0091aa569))
* **update:** 拖拽逻辑修改，接口修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([569fc02](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/569fc02f9378507eedcd999bd5d65a254e72edb4))
* **update:** 左侧树拖拽之后，刷新tree[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([4173933](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4173933db44ec2552cbb7babc4bc8f4376b39846))


### Features

* **定版:** 1.0.1定版 ([13891d7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/13891d7fd097cce35121da9667513b05d86acc79))
* **定版:** 1.01定版 ([8f03fe1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/8f03fe15e8dd6d34ef029848a86bb0d7aa3d0931))
* **升级:** 重新生成yaml文件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/7)] ([3cbce74](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3cbce743cb3a7ff62546981da27297f7451658e6))
* **cli:** package-lock文件提交[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([9f37092](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9f370921f4dd15533377b8ec432d149bc5500ef8))
* **locales:** 国际化方法修改[[#10](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/10)] ([ef3aef4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ef3aef469c19a2a90ae28339a6ecd48175e80557))
* **menu:** 菜单排序调整[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([7bbf625](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7bbf6251ed90511be7899de514b3e73c5ec45173))
* **menu:** 产品文字改为服务[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([320726a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/320726a941cc4472c4ccf23a957d8b98356f0be3))
* **menu:** 提交packagelock文件[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([10778cd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/10778cd6d61ed6a422b7dabb4459260be350d3f4))
* **modal:** 弹窗样式修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([ad9363f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ad9363f260433dc7f3477ec421465132d9162d5b))
* **vue:** 依赖提交 ([db93f0f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/db93f0fd11b75c0e5b07d70cfa1d637d4aef0cd1))



# [1.0.0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/compare/28659bb098e260eee3fec6522782483d5defdf0b...v1.0.0) (2022-08-30)


### Bug Fixes

* **代码合并:** 代码合并 ([f05eb3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f05eb3fad7dfdaf793b6a2b4191fd9502bf2f866))
* **admin:** 主子应用打通 ([7afb056](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7afb0562c91be650cd3fb38fb79e15868bc2506c))
* **ant:** 更新ant-design-vue@~3.1.0-rc ([4ca2bd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4ca2bd307e68b797e96d8a9269cdde02ba795435))
* **antd:** 调整vite依赖引用 ([74942d5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/74942d5abe0b9579966d859f542fc6bb9c264d81))
* **antd:** 暂注掉antd图标按需加载，后期调试 ([c5b4766](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c5b476687c631a14dee0540080843dd7a90092ec))
* **assets:** 支持动态加载微应用资源 ([a121b57](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a121b57a0fbf949d3f4efc44d989dcc3a1e95449))
* **axios:** 修复axios格式化数据 ([145baff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/145baff06da0a57e5b765b6ec85c21246923afae))
* **axios:** 修复axios格式化数据 ([1c98fbf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/1c98fbf1e59027e16ef328dee9746e6b1f19eb82))
* **axios:** 增加axios微应用代理前缀prexyPrefix[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([76b6650](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/76b6650d43b09fbcb130bb4941729dec562497b0))
* **babel:** 修改按需加载引用目录为es ([38ac102](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/38ac102a89be1dc747856862090935197065fdb9))
* **base:** 根据开发规范修改文件命名[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([d03badf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d03badf4aea253280b40eaf16e7fcd88208a3299))
* **bug:** 修复主题，登录等bug ([9116e9b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9116e9bb7524f8fe1b2f25870694c2f930baa002))
* **build:** 调整项目打包配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([8a20fe0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/8a20fe0fb4a0a3022dce708c5fe28ed9ebefbd5a))
* **build:** 去除sourceMap[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0f91481](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0f914813b0e422cd8a26fafb8c9cf006a9483907))
* **build:** 剔除monaco打包处理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([01b92ac](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/01b92acff392264ed061de55542104569796cba7))
* **build:** 提供公共资源的json配置文件，方便应用调用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([3122798](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3122798d67192219d4cf2cc44b25c6f98d2845bd))
* **build:** 修复微应用打包后的数据文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([bc40d94](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bc40d94ba4547e7e047933cf44e9ecc042aac8a9))
* **build:** 修复env配置文件 ([0044821](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0044821ee5f3107e1998dbac970fdf46524a727b))
* **build:** 修改webpack打包配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([395061c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/395061c23f9612d753f930006957fa7c04e2ba98))
* **build:** 优化打包流程 ([3cf58a3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3cf58a361e80f2fb3f10d39c157bb26a9c6ec7e0))
* **build:** 增加图片资源打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([5ef32f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/5ef32f6b8deadbc6a526daac23892e06366c82c7))
* **build:** 主子应用打包使用相同配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([63ed9d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/63ed9d34fd536af080e46e41c8b8b37e10af05ee))
* **code:** 调整代码结构 ([3ef4c2d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3ef4c2d69dde2428574985b7c5b3c68fa312824e))
* **css:** 统一调整antd个性化样式引入方式 ([d7d1bbd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d7d1bbdc3166a0de1982d75029428adae67de525))
* **doc:** 由于pnpm7不兼容yalc，不支持file:xxx安装本地包，所以请使用 pnpm6[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([c461e36](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c461e36575353d9814b02e5360f08e1e562a3b25))
* **envs:** 修改系统变量 ([015b6a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/015b6a21b59a90e0cd7f89c531edf8085b231d06))
* **eslint:** 增加 'no-case-declarations': 'warn'[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([08e2b46](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/08e2b46ade750da82458707642d4cb546e04c5db))
* **file:** 更新pnpm-lock文件 ([b7aed24](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b7aed246df4955c07883568d910d300a513aad67))
* **file:** 修改App.vue改为app.vue ([b3c5cb5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b3c5cb540044670a06bf42414afdafa58777070b))
* **i18n:** 抽离监听国际化的方法[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([c4506b4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c4506b49d16f8e17f4629e1ddeee15c80226d035))
* **i18n:** 调整国际化，增加使用说明 ([65947d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/65947d92303588185715f8711fc58a56d4e96591))
* **i18n:** 调整i18n代码结构 ([26768ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/26768aee6fb6294c7462923af3a65c46b16c4c0d))
* **i18n:** 去除i18n打包配置，导致微应用异常[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0bd0cec](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0bd0ceca303cbb87c460607bde6a96ad49b05ff1))
* **i18n:** 修复国际化问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([352b873](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/352b8734fa951dfabec3e611812f80f5d7c2dac1))
* **i18n:** 修复i18n旧模式下api兼容 ([450d2cf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/450d2cf1a1f23ca02c85b871e2cf4436c25fafbd))
* **icon:** 更新图标文件 ([732a444](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/732a44439fce9a759abd67bb82ff8bbc56a73592))
* **image:** 删除无用文件 ([28659bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/28659bb098e260eee3fec6522782483d5defdf0b))
* **index:** 修复初始化配置设置 ([64d3c8e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/64d3c8e813b53b99213fe23ddd0b3e08c2fa807d))
* **index:** 修复打包错误 ([cf96bfa](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/cf96bfa76a11751356fb44cea55b1edc412164d9))
* **inputNumber:** inputNumber 限定只能输入正整数 ([2b6a141](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2b6a1418e0c80c89b13445ea1bc2a4b4ba36e399))
* **je:** 调整je按需加载 ([6da9c79](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6da9c793339785e7f7dd7628224106fa0379e2ba))
* **layout:** 修改路由容器不支持滚动条 ([46a5b04](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/46a5b0413141d68e659cd32c63fa2c95f444fe04))
* **less:** 固定less版本3.0.4,防止主题构建有问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([03b82b6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/03b82b6cbeec0603824c19f9e2c81d3b76541408))
* **libs:** 更新libs项目[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([e0df5f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e0df5f61d16a63e1b7ac29dfbf4f53b931991be0))
* **libs:** 更新libs项目[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([f8809bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f8809bb47660553bad5a9e69c45f05c68c4b69da))
* **lodash:** 修复lodash工具包依赖 ([9dd17e2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9dd17e20446fa90b7503eed1fa4070c2b3d2c970))
* **lodash:** 增加lodash工具包 ([6c13199](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6c13199b0eae2484ba16d6fe4f1b6a5c6d246ad7))
* **login:** 抽离login方法，提供全局调用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([729a2c5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/729a2c548229491a6a5242fb2798fcb6945f19a0))
* **login:** 微应用登录页丢失，默认使用系统登录[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([15735dd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/15735dd58155ee5441fcae9fc266693abc398714))
* **login:** 修复登录成功后，路由跳转问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([a66b4c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a66b4c0323c788c752ced8d837a83c891023b601))
* **login:** 修复登录后路由跳转问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([2ea0c62](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2ea0c62add33877bdc995bb27d9623019904c870))
* **login:** 修复登录相关操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([4747fcd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4747fcd3b915c28606fc070649db627b0ed0953c))
* **login:** 修复用户失效，退出登录 ([2eed3ae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2eed3ae3626937f7769284d7bf51897c8b41af1a))
* **login:** 修复主子应用交互混乱问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([e42b4ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e42b4ffda48cd7afaaeb3bf3fb4f4a87331e8b14))
* **login:** 优化登录页同主应用保持一致[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([39587c9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/39587c989a192aebfa2d42d224fd4c9c9a19968c))
* **login:** 优化登录页同主应用保持一致[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([15cbd0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/15cbd0e5c1fea45fe97f874a11ab4ab733d628b1))
* **login:** 增加网站备案号 ([f8641f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f8641f71111a5cae697ca8398cd5f158e13f1142))
* **login:** 增加login样式 ([7a0db20](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7a0db20513807519bfa04048074ffa3b639f1689))
* **logout:** 将logout从JE抽离到system ([abf67e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/abf67e05098cc8f85c478294ca8de9c34a875dd0))
* **main:** 修复入口文件引入[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([f389efc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f389efcf12c4b90d8160140136f02b14602bd724))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([a3556d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a3556d3b9e4104d96f151e9d2eff2053c9f9ece3))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([e503226](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e503226fefc6a2c6affacdbe5863bf1fbf32095e))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([9c7c83f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9c7c83f83f8bc9d34cebac9afe310b01364bf4fa))
* **menu:** 顶部菜单关联tree数据处理[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([e3c4dbe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e3c4dbeca1916afd8af2d744e2c22291ab511adf))
* **menu:** 关联顶部菜单的逻辑及其后端接口对接[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([9562e03](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9562e03d7efa4d9248e81198c810a7760bf747ca))
* **menu:** 解决菜单树右键传值错误的问题 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([40a1970](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/40a1970befa4f79a0845cb76fc4c525f3fc1c817))
* **menu:** 数字来源、更新条件修改默认值 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([7036ad2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7036ad223a12e914d34f9e1413ae028512317771))
* **micro:** 调整微应用代码结构 ([91501cb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/91501cb6a6024d28583e712cf5da1e4d382c3f8f))
* **micro:** 设置登录后的系统信息[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([8facb67](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/8facb67d32b666828fc06998ce44380a6694e8e9))
* **micro:** 设置子应用共享主应用系统数据[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ebc8432](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ebc843288f4d94d8946022ed7b2de4de07b5f283))
* **micro:** 修复子应用注册登录成功事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([f542360](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f542360d7f67dc2e3b3ad49f56b84dffd4b54fa4))
* **micro:** 修复microStorebug[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/45)] ([16dda0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/16dda0effefd2b376e533ad44d9961ebcde43916))
* **micro:** 优化主子应用交互方式 ([e4b9be6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e4b9be68f5b458310b2f5db509a0ac65865c298b))
* **micro:** 优化子应用打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([e3a93c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e3a93c248e3d672dc532c42dfc41c8ee04d21226))
* **micro:** 增加微应用控制[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([bebda01](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bebda019b930f198da1d354480a723a29efdf047))
* **micro:** 增加微应用控制负责度[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([7e9fd5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7e9fd5fa3f4091a2298d0c35bbb5a25eb4a59673))
* **micro:** 增加微应用store的name属性[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0ef5917](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0ef5917ed71601dcc559709e6b62ee90e33f44a4))
* **micro:** 增加主应用标识判断 ([a71ee5d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a71ee5df08d2017d5adf2c198a096ac021dde80c))
* **micro:** 增加子应用激活路由配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([44ea958](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/44ea958233db96c2810d3a0a46619a441c0e44e4))
* **micro:** webpack打包主应用不处理output[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([f3dc8b1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f3dc8b1dd5f3573fa4d4ce5efbb018ba42ca22c5))
* **mock:** 修复mock例子中的错误单词 ([c7f67f8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c7f67f8fc7f9de63b6e76d8e5a469a084643993a))
* **modal:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([ec37e83](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ec37e83b10b572b2794c40c201962c8305e36680))
* **modal:** 修改页面 modal 组件 ([4117e95](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4117e9599b9d2d3afd2037d16f3b361ce2fca0bb))
* **monaco:** 去除monaco打包插件，通过静态资源引用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([dddb641](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/dddb641dd444d319df243ba5095be33057b54a3b))
* **mxgraph:** 修复源码，将所有属性注入到window，兼容沙箱模式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([44b6b3f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/44b6b3ffcf8dfdf4a7df8389373d5b489795dbc5))
* **npm:** 更新pnpm-lock文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ab75426](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ab7542619055c2599fe514ae398d52248ebbdfe4))
* **package:** 更新所有包的依赖[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([f60ecd3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f60ecd323b10533ad62bf689dff0c7aeb7a31a9b))
* **package:** 更新ant-design-vue版本[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([89efa95](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/89efa95a824e42ecfb4cbc2203e6b53c2e32b24c))
* **package:** 更新libs包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([2c42413](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2c424139741cc8f559bdb74937a78b5c8477c20f))
* **pinyin:** 修复pinyin-pro依赖引用问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ef71e00](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ef71e00761753aeba11233596c8dda6967d7d468))
* **plugin:** 增加plugin插件绑定JE[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0caaf66](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0caaf6687fd90a50f7829f8372e23695d837e99d))
* **proxy:** 只有在主应用才会启用子应用的调试代理地址 ([8def26b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/8def26be5a7add2cc7a3996796bc0be75ac140ee))
* **route:** 修复子应用菜单获取路由数据问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([4a61aff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4a61aff54ea364a48a79b4e3831e65071bc1415a))
* **router:** 调整路由，路由守卫统一交由common管理[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([2bcc274](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2bcc27495ebc6f571a1827e89d29caa87ecd9c43))
* **router:** 支持系统路由自定义配置，支持路由菜单 ([bcb37f7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bcb37f77b2076b9d23ccfaf1f50cacaa5c373af9))
* **settings:** 调整系统设置按钮 ([41a8e2c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/41a8e2c09c2562ba580ae62a177b25ace854bad6))
* **static:** 修复由于主题插件导致打包失败问题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([4f80a7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4f80a7ecb794eeac3efde7ea4bbb8179b184ba76))
* **style:** 调整系统滚动条的样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([2d849a5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2d849a583b44b82ba8ceea8b16a714b716c38448))
* **style:** 调整scroll，button样式 ([ba0aaca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ba0aaca540345aade047b83300d629598ba93dfe))
* **style:** 开发模式下微应用不加载样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([19e0bd8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/19e0bd88ac37002f2e8e3631cd10f7b255a91f84))
* **style:** 修改style文件引用路径[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([e3df4bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e3df4bc77cdbd6444cc3f6d6b77cafbf0117ce6a))
* **style:** 优化ui库的资源位置 ([c2890e3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c2890e35fe680260c5623f41cbc95e7bf39b83d4))
* **style:** 增加ant tabs样式调整 ([bf61f69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bf61f694e5fdecbf033d239d6fd31e5aa24ba542))
* **style:** 组件包的样式引用修复 ([ef5fc45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ef5fc455408a91e5a42b0e4377b2cb2606baee1c))
* **system:** 系统的方法改用@jecloud/utils里的initSystem[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([b00e589](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b00e58957e732a252f23e550e5b01139f79c0bd8))
* **system:** 修改系统变量请求接口 ([b80e4e0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b80e4e0bfa3dfa9a2988d51aa34ad1a88385aebf))
* **theme:** 表格主题色调整[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([2b80f27](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2b80f27e71f4b8082a1af54450b2905a14cee71d))
* **theme:** 调整表格主题色[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ba82c8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ba82c8b22e7a46a851c39c939e340e2e892e9c6f))
* **theme:** 调整主题打包文件[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/6)] ([b046aef](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b046aefd0edd171fded0db61f33d7613d7eab051))
* **theme:** 将主题变量改为文件[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/7)] ([2c587f0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2c587f041935cff2ef7ed4b6b1cdb76284f5922a))
* **theme:** 确定主题支持方式[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/6)] ([cb8d445](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/cb8d44556dde96e1ed7de7aa33f48f8ad4d9c028))
* **theme:** 修复代码构建主题文件目录错误 ([adf0bc4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/adf0bc472a68697dc338cc7beec2afbb19c18d9d))
* **theme:** 修复登录页的样式 ([5d416c4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/5d416c415d615fa1e66445d872a774d96c22435f))
* **theme:** 修复页面头部主题颜色 ([d9376c0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d9376c060ce3cd383f53f39215e6238019be08a8))
* **theme:** 修复主题色样式 ([8383043](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/83830436cfb35570bf3a88667e269d86689db101))
* **theme:** 修复主题样式 ([a6fdef7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a6fdef7afcf7becc1b109ea669db9e87c0d16d15))
* **theme:** 修复子应用不设置主题[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([90e4475](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/90e44752431eef8a828e509c540686f7ad0f2f06))
* **theme:** 修改系统主题默认字体色为[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)f3f3f ([c5f0e6d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c5f0e6d17fc12ae7ca4ae14ecbd8cbfcfaad7306)), closes [#3f3f3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3f3f3)
* **theme:** 修改ant主题变量引用 ([c75eaa4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c75eaa4a87bf86f7e0362b2c2b5e28fc1a43b705))
* **theme:** 优化主题色选择组件 ([a645eff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a645eff862ac9a71a0ec2084a62e6ceb7620c2de))
* **tree:** 树的启用和禁用使用判断条件更改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([4f73a9a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4f73a9aea592de471fcd0aa21cafc95d1a8c995d))
* **tree:** 树的启用和禁用使用判断条件更改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([d2872bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d2872bc547c813a32d3353b42e8b00c252ac4d73))
* **ui:** 绑定项目publishPath ([13f7d13](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/13f7d13cf32fdc156d5ac67e98039eea0e1aa1d1))
* **update:**  关联顶部菜单[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([aff5be0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/aff5be0a37082f323f1e7ce4c2ec28dac7d51d7d))
* **update:**  解决语法报错问题[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([af2fe6d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/af2fe6d654d3402b8f88ed59aecb9a23ff009d9c))
* **update:**  优化选择功能[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([4dde836](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4dde836e2ef2790c23f327aa987a2fa5b3210d82))
* **update:** 菜单部分bug修复[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([e7bc2f9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e7bc2f9b406ffdcfc2ddaf0b92f70711d88cadd3))
* **update:** 菜单插件新增逻辑[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([57db712](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/57db71242bebf99d86f4a46f1900aff61656ef04))
* **update:** 菜单代码修复[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([6d83800](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6d83800c15df391350912fee034e1b7e761b67e5))
* **update:** 菜单搜索赋值[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([22a8899](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/22a889951ccd697b3e5204707d090b6b97b41e47))
* **update:** 菜单添加产品逻辑[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([cbe43c6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/cbe43c6f7e6b24e589e01cff91773bfd690171e2))
* **update:** 菜单选择功能[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/16)] ([710af1f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/710af1f4a8be23e298d1e7c12e6f3f5955238d10))
* **update:** 菜单隐藏部分配置[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([188d304](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/188d304931e7e6a523e03fa3749a5e425b58aa70))
* **update:** 插件备注修复[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([3a24f7b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3a24f7b65bc59bf17cd27bc956e2b408d1259dae))
* **update:** 代码提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([baf3018](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/baf301895d26844c9b4060abc97534a78a611a7c))
* **update:** 点击关联顶部菜单图标问题[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([836d548](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/836d548ec373c1d49e610f6c0f7f8e60ec95d185))
* **update:** 顶部菜单字段修改[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([9c56325](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9c56325d78bfb070d0d9b35aa078c3bd2022fb90))
* **update:** 解决菜单tree拖拽问题，加个菜单数据不显示问题[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/16)] ([44e0a15](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/44e0a15f806736724acdd0aabd3751ccf8d8e207))
* **update:** 解决产品选不到问题[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([e3891bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e3891bb8fdd18af078f3301b28071d8e1a1457f1))
* **update:** 解决升级语法问题[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([6d2abbe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6d2abbe6fa27193531331f33b2e3a5e43d41aeb4))
* **update:** 解决线上升级报错问题[[#16](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/16)] ([dc6ddf5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/dc6ddf5e6aba4e3e9fa9390b5c39e991a7bad139))
* **update:** 流程模块字段处理[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([7a0dabe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7a0dabeb8258e1e1ad4bcee3ac6b3ebd5c783650))
* **update:** 模块和菜单中国呢配置信息字段不必填写[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([c393110](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c39311010b0aa9e4eb6b4d3e7f62d916c55dc478))
* **update:** 去掉菜单导航字段[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([45700bf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/45700bf92b577630d373d1fc92ddcd3a997cced8))
* **update:** 去掉jeicon样式[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([c1c4df6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c1c4df614dd1936986cdb7d47843f6569b663bf2))
* **update:** 授权[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([5dcd599](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/5dcd5999909793be1a5b81490d4c90dd308246da))
* **update:** 拖拽[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([b3d5b5a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b3d5b5a1d4e1422f5c4d88dc0adcadaaf5932341))
* **update:** 拖拽问题[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([64f7510](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/64f7510a683bd80d9c2f18500778451c7dcc7c35))
* **update:** 显示菜单[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([e8cacf9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e8cacf9e6307bafa92b8bc63c50d02ec27f22aab))
* **update:** 颜色放开[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([595a028](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/595a028f790b6a2a566102ccc5c097482da61947))
* **update:** 隐藏菜单项[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([636e956](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/636e9566b62d55db9df6a327a26e351652ab7c18))
* **update:** 隐藏展示树形表格[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([d8533bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d8533bc063f12cd2e14a927c129777b249bbaff1))
* **websocket:** 修复websocket监听方法[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0183ee6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0183ee689d59f085c209e764512f7f0e94638372))
* **welcome:** 修复欢迎页的背景色和字体色[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([2ab3122](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2ab3122ee23db703b8d40ed28eeab8da81980c91))
* **workflow:** 移除@jecloud/workflow,增加@jecloud/plugin[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([e69cbe2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e69cbe20caa548085bcf74d28a83a911d4596673))


### Features

* **菜单代码接口切换:** 菜单代码接口切换[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([0822353](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0822353e04bf711da6695abd4b8da1aa0c4dc1bb))
* **菜单树:** 菜单树样式修改 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([525c3d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/525c3d9f6847512f3cce23abe36e478a6c700bac))
* **菜单树:** 菜单树增加 loading [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([874ba79](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/874ba79ad24cd264ea0d835ca56526657159451b))
* **查询选择:** 查询选择修改优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([093a550](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/093a550675cde68945438ec3f3b8797267a5a722))
* **查询选择:** 查询选择修改优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([0a50676](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0a506768b8195276f83a7539de03cf18f763f681))
* **查询选择:** 查询选择修改优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([b7e6e73](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b7e6e730e4d14481c94d4d2015c6fda89630ecc1))
* **产品列表:** 产品列表接口修改 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([ace303e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ace303e14e6fb42c60bc06773832c570973d6ed9))
* **打包代码提交:** 打包代码提交[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([5c5143b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/5c5143b3878518eaada4b1939b0412c4a73f0690))
* **代码合并:** 代码合并[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([6c69be1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6c69be1e596c78a7177802bd4f58a29cabfd9544))
* **合并分支:** 合并 feature/[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1) 分支到 develop 分支 ([f2669f5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f2669f596a057d079d1ee422094aa16ae93cba0b))
* **合并分支:** 合并 preview 分支到 develop 分支 ([b10a69f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b10a69fcc4e1be39f56800d5c3f27a371686e8a6))
* **流程模块跟换数据字典:** 流程模块跟换数据字典[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([e457609](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e457609ff0bd43e99d70e71244e95c760e92cc98))
* **字体图标:** 字段替换[[#2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/2)] ([a3add6f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a3add6f7c664496fc640588b9507dd85a19d583f))
* **ajax:** 统一前后端数据格式 ([937fd40](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/937fd40adfcb81a15aecbf02087579fa56413a6f))
* **ant:** 更新antd版本3.0.0-beta.9 ([92a29e8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/92a29e8b69eb7a5c9797aa377fca8c3feba0fd85))
* **antd:** ant-design-vue升级到3.1.0正式版 ([5cd3f38](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/5cd3f380bd898364663e2905d8a85e86c630d689))
* **build:** 增加@jecloud/workflow样式[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([9b66b09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/9b66b0909306d745bce60b9ab5170b7896d1eea5))
* **build:** 增加打包去除注释插件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([66282ff](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/66282ff1364b2d653d28f71e612f11c341ec612b))
* **build:** 增加微应用相关配置 ([137bc7f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/137bc7f5b5173a66a1b43a81301cb904a6fcd05d))
* **build:** 增加文件配置和路由守卫自定义入口 ([751b820](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/751b8203845909638919e2852ecfbf6e42eeb0a3))
* **demo:** 调整入口文件，增加demo页面 ([0d13adb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0d13adbb6a399296dca58d19550783214b0773c4))
* **doc:** 增加开发技巧等说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([06b95f5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/06b95f5c5dc6cd764587725e2d2f5265e7e5e420))
* **doc:** 增加开发技巧等说明文档[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([f412d5a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f412d5ac1bddeabf7859265f4facd41785e122b3))
* **doc:** 增加git操作技巧[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0eaf65d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0eaf65d1c1cda085057cc566b73f3a7accbb60ce))
* **doc:** 增加websocket使用说明[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([6f0d2ca](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6f0d2ca85dc4cd573ca59c7ef436c8cd13156fbc))
* **fonts:** 增加字体图标[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/4)] ([6e87e81](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6e87e81c1d19dd1905e5f522a75a515809912801))
* **func:** 增加jecloud/func功能包 ([2a24cb2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2a24cb2ee1a9bae835b06043edf3fdbdd5f5e3da))
* **func:** useSystem增加功能操作函数[[#45](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/45)] ([62502c1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/62502c12bcd9e077b57e5bc2b6e400978bac170d))
* **i18n:** 国际化初步 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([d7165ea](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d7165ea69e831fd0e6d89168a2d8240f119201b3))
* **i18n:** 增加国际化支持[[#3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/3)] ([cfcc144](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/cfcc1448f85c829dbcb46d01b9f575362d3ea376))
* **icon:** 增加icons路由，去掉icons.html[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([7accc7d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7accc7d1fea492d0cbbd86b47c609388409abe69))
* **icons:** 增加图标使用帮助[[#4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/4)] ([e7b5895](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e7b589560f172b718cd7be07b34f061d8b84fcdf))
* **icons:** 增加jeicons图标 ([37c1055](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/37c105591bb681c37725774048244ddfd085657b))
* **je:** 功能组件绑定JE对象，用于事件操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([4efc10c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4efc10cde1099b2e88d49717d960c628295ee376))
* **je:** 混入JE系统方法useSystem[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([eb627a2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/eb627a2e773b4a7d220260701a170e8703c9903c))
* **je:** 增加watchWebSocket方法，只有主应用可用[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([20e0a0e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/20e0a0e1a897fb2b1eb488eeb8cd51e794982033))
* **login:** 优化登录操作[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([45b29d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/45b29d24bba9930bee9a1cf6b9c5b41e83a6519d))
* **login:** login增加部门选择[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([770df7e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/770df7ea8a47ce2881b06ed2c0bb2ff9fbfa074c))
* **menu:**  菜单基础文件提交 ([ad07c8b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ad07c8b51c7510ee72f7610688d86737a6166fa3))
* **menu:**  菜单基础文件提交[[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([b5669ce](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b5669ceede4984066a4615d934e9b81ce126ec70))
* **menu:** 菜单树接口调试 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([7dc9824](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/7dc982428f149cee76352e48ec0a49c45245478d))
* **menu:** 菜单添加接口调试 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([b44ab83](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b44ab8301e4a674be67e77c1af6526c935365b2d))
* **menu:** 菜单修改优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([54ec979](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/54ec97942f6ec587aeb20ec61bd27c39b85ecc03))
* **menu:** 菜单样式调整 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([270ce19](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/270ce193b77b858c201a9c55bdd82ae8432a8432))
* **menu:** 菜单优化调整 ([3315654](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/33156542076cbc96131340b808f540f72e04a96f))
* **menu:** 菜单优化调整 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([0092432](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/009243204f020df025fb579646af0e9fd1ec22fd))
* **menu:** 菜单优化修改 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([c97faa2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c97faa29ab85cbc557222dd899b62b4cca80fb4d))
* **menu:** 菜单增加删除功能及优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([552b647](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/552b647d36697ea69c362cb3535eadb6124863e9))
* **menu:** 初步开发完静态页面 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([51fa524](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/51fa524183bac87bf5046753970fcdc5663df713))
* **menu:** 代码修改优化 ([2fee210](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2fee210fdc21cffff0ad96394079af397d245fd2))
* **menu:** 代码修改优化 ([6f49ac3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6f49ac35644ef16a0d3d6e5ad7cdbf4b2e33cdaa))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([ed0fec9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ed0fec9e07e1cfd9abac9384a629248d34d71ec8))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([0216b41](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0216b415ac448df76ce8899af97d674abb3553a0))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([624965e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/624965eed347ac0955a099e92ea714559c338b88))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([c2ff3b5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c2ff3b5e72fc4e47ba70f80b97e1e00d05c4dbcc))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([c72fef1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c72fef113ea9a25cfc722053b55fa6ca69ea79bd))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([cb02ed4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/cb02ed4d570f021ea741083d64adbb5f12bd7abb))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([50cef07](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/50cef0782e72e8b0a610a52ba36deb628e793182))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([c2ba58c](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c2ba58ce14afdac7d6365019aa8807d3a4b7faa2))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([aa5ce7b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/aa5ce7b1d0f2137cead832f7f1e506785fe3c0c2))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([bdb173b](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bdb173bacd2fe86bdedeed3e6b39bcafe6206bb3))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([c3d0c0a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c3d0c0aa16da7f2f022f748623f7935fabb1064c))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([4a6d35a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4a6d35a2d91defda37351947ce045bd47997dd2f))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([a63bbfe](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a63bbfe76b78eaf839302cfd71696f11ac4df65b))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([18af2a6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/18af2a69aeee6e63ecb427142ca2d468e3bc9e49))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([03f5aae](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/03f5aae0dc6d8e3d697cc259b7ffa5b3fdf829e6))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([15be220](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/15be220129ea5d91d7d2efe5bccb5436ef83d540))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([75f7960](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/75f79608d00e84ec49501ce7c4b712c2949357cd))
* **menu:** 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([709968d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/709968dea49bf70ee65777f7dde4690bb7474c5a))
* **menu:** 代码优化调整 ([ee73d05](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ee73d05496b2b8ec36b9b6b197dc0fcc0b401dc1))
* **menu:** 代码优化调整 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([b6da81f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b6da81f6e7ce44e8fe0e3837476eb63d3c249154))
* **menu:** 代码优化调整 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([b0bc936](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b0bc936cb56623912a01f22339b5af00dff9ca47))
* **menu:** 弹窗优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([2160b53](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2160b532118e9fd2952d39a47f337e05db08d952))
* **menu:** 调试清空缓存、授权给开发人员接口 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([fc0c668](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/fc0c668af59fa48092f8f2bac015aca166dc2a04))
* **menu:** 高亮初始化的菜单的第一项 ([247077e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/247077e3a19b8e88e5889a9ee7ec9b8346289a45))
* **menu:** 高亮新添加的菜单 ([5513347](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/55133478920dc822ff5949e30e28317a22b8a3e5))
* **menu:** 更改菜单图标时，菜单树也随之更改 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([1664c95](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/1664c95aad93203ef6677a13e2f501397ee9e98f))
* **menu:** 功能菜单优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([646af7d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/646af7df4f726386104a185abffce0576bd30bf1))
* **menu:** 合并 代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([0e05173](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0e0517330f24b38a85e3e9665dfe3825925810c5))
* **menu:** 合并 cli 项目 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([d3c6cdf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/d3c6cdf2668b19bf3c7250c88130655f8dea377d))
* **menu:** 合并 cli 项目 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([c814f61](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c814f6157fa455ada096a3dde5df9d27a5808807))
* **menu:** 合并 cli 项目 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([a9cc287](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a9cc287ecb2ffcaebd7cfa46b3a9327c0d7dd03f))
* **menu:** 合并 cli 项目 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([f3091d9](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f3091d9c0976350217a0524891893b7a7ceec9ea))
* **menu:** 合并 cli 项目到本项目 ([779d9f6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/779d9f68265b0188c4eb2247ac861315857c4e5d))
* **menu:** 合并 cli 项目到本项目 ([bc5f827](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bc5f827a8b2ae40c3961f5662fb9f4abb4ddb566))
* **menu:** 合并 cli 项目到本项目 ([2110be2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2110be27053789019d8db551c940f56e898bb836))
* **menu:** 合并 cli 项目到本项目 ([caed0a8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/caed0a83c18be13b36b8b0559ef3714e5732e6c5))
* **menu:** 合并 cli 项目到本项目 ([2c77f27](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2c77f271814c35d15891d36c23b199f6808c787e))
* **menu:** 合并 cli 项目到本项目 ([6a148d0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6a148d06d8e2179fb795016bb169313d00716d5c))
* **menu:** 合并 cli 项目到本项目 ([00886d3](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/00886d35423517a4690c6223f2f05e1102abad06))
* **menu:** 接口优化调整 ([3df8a71](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3df8a713d3027ad945f707d5bd8bbfdcbad431d9))
* **menu:** 接口优化调整等 ([54db207](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/54db207853ddeed2cb8c11cb824b86f791ac3ad3))
* **menu:** 使用 penal 组件代替，layout 组件 ([7757933](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/77579338a7280f70f04c180646650224f544f8ab))
* **menu:** 树形改为双击展开 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([70f7863](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/70f7863aa79bce6fd0a5e10f50d80c4526b403cd))
* **menu:** 添加菜单接口调试 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([a06ea21](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a06ea214992907a877044771e2bb54348fa79795))
* **menu:** 图标、颜色、form 组件更换 ([f557159](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f557159b2d718e995ebc0ddf9d18ab49492887dc))
* **menu:** 拖拽接口调试 ([f31f6cf](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f31f6cf3a9c3f76f7a34c8bd479541b37c317357))
* **menu:** 修改字体图标 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([81fdc65](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/81fdc658dc328888fd1ccb475b0e236c89d00a52))
* **menu:** 增加产品切换，代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([f593553](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f593553abd95b829ccdef46e3f46f207d63227ea))
* **menu:** 字体图标修改 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([6e36cdd](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6e36cdd6254e221be4d61ac5593845e04c3adc0f))
* **menu:** 组件替换为je [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([b63984f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b63984fd00e862e4fec09258406bbbcaa59f5999))
* **menu:** 左侧菜单初步开发 ([e25ff09](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e25ff094ab0c443a38ba2ff2f9354236529a854d))
* **menu:** form 静态页面开发 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([172f4c2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/172f4c2946e50e549cae88f3386d3e2734bd2490))
* **menu:** modal 组件修改，代码优化 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([705e676](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/705e6763479de0b0089642e427c9c7338cc1e121))
* **menu:** tree 组件更换 ([ffcf1bb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ffcf1bbb18c107673f8d64b6f04311de4d271fea))
* **menu:** tree 组件优化 ([c745590](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c745590f730167cdc458091939edca498085a55d))
* **menuTree:** 菜单树添加点击事件 [[#1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/1)] ([1b41e4e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/1b41e4e8351c98ebc715723468cabcfe243bbf33))
* **micro:** 暴露micro-app的钩子函数[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([59e609d](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/59e609dbc541d7e15f3e6a56dd347309c67a5cc4))
* **micro:** 调整微应用框架qiankun改为micro-app[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([23630f2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/23630f21e7c79f380806caf9ac8424e9f7f9b9c3))
* **micro:** 微应用支持触发其他微应用事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([0c54bf7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0c54bf77ae1733b1cccef5d2b45419bd31269b3b))
* **micro:** 增加主子应用通讯[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/7)] ([2d7b3d2](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/2d7b3d2fd72563184e8adadcc78abe90175521b2))
* **monaco:** 增加monaco静态资源，不再单独打包[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([c0961b0](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/c0961b04dd161afc33a9e48a4baaf5e111888bee))
* **monaco:** 增加webpack，vite的monaco插件 ([1df85bc](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/1df85bcf34dd7a68888de72026762dc6d05cf99d))
* **monaco:** 重写vite插件，修改路径引用错误问题 ([dea79f1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/dea79f1790d42e4e896636112319ae9f4627b1e9))
* **mork:** 增加mork数据支持 ([3eb1d44](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/3eb1d449abfea042263f3d42f091c001d23138c7))
* **mxgraph:** 增加mxgraph静态资源文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([e4cdc85](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/e4cdc853b6db5d8f34ab2bd3c34a5ef1f07f766a))
* **plan:** 增加globalStore的方案配置[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([a335bb5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a335bb5c9eeadcec12f55e17a97a1bb8ce0422b4))
* **preview:** 增加本地预览服务 ([0cd7aa1](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/0cd7aa141fc756e900323fb142fe62d69271844f))
* **preview:** 增加本地ip输出，方便调试 ([4afdc69](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/4afdc69fec00fbb0ff1daf85a368be9cec75c83d))
* **rbac:** 更新登录和获取用户接口[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([83a9597](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/83a9597c021c1c91e21f2e77a4614a675aaa0186))
* **router:** 支持用户自定义history ([1421921](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/142192104955f238e8112d959c1d55a35f1318fd))
* **router:** 支持自定义路由白名单[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([1ee13a4](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/1ee13a47dd250e2ea4ca2a3367d8bb455388a078))
* **static:** 静态资源文件提取[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([f3e0edb](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/f3e0edbbe6a20f33922449e56db057e37b777396))
* **static:** 增加静态资源包文件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([28e62e5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/28e62e5b23fa933a368473623c2d152fad5e5d7a))
* **theme:** 增加灰色模式，弱色模式[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/6)] ([fa4d1df](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/fa4d1df85b69176cb5956e52ac77594b5f8307d2))
* **theme:** 增加主题支持[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/6)] ([bbfa033](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/bbfa0333e212e51b73355b4bb948f41933c5064b))
* **theme:** 增加vben主题设置[[#6](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/6)] ([ad34b2e](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ad34b2ebcb496b8e422421bd940af99ab4666798))
* **theme:** 主题组件增加切换主题事件[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([ef9ac87](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/ef9ac87c6ae84c2f14de177d0ed89312ca7a0f30))
* **theme:** vuecli支持主题配置[[#7](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/7)] ([b361a5f](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b361a5f0e2e976884a25e4494d2fa003b8a583ce))
* **tinymce:** 增加tinymce插件，更新antd版本 ([13f390a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/13f390a11f4d131177ab94cde91ef694ace3a2a5))
* **version:** release v1.0.0 ([b146b3a](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/b146b3a337cebde0abae9ff97d57ced3ae21c33f))
* **version:** release v1.0.0 ([a969009](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/a96900905fea3ce679206313ddd57c2ec0504011))
* **workflow:** 增加@jecloud/worfkow[[#8](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/issues/8)] ([6d1aea5](http://gitlab.suanbanyun.com/jecloud/frontend/jecloud-core-menu/commit/6d1aea5c6719b6115ffe84a89c6f9b0c28861267))



